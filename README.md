# LEMP-SSL-WithReverseProxy

คอนฟิค docker compose สร้าง LEMP stack
โดยรวม SSL (จาก Let's Encrypt) และใช้ nginx เป็น reverse proxy

# config & startup
- เปลี่ยนชื่อโฟลเดอร์ `website` เป็นชื่อที่ต้องการ
- เก็บไฟล์เวปไว้ที่ websitex/app 
- สร้าง network `my_webproxy`
```
docker create network my_webproxy
```
- รัน docker-compose ที่ nginx_ssl_reversproxy 
```
docker-compose up -d
```

- เวปแอปให้ตั้งค่า และเรียกใช้ docker-compose ตามปกติ
```
VIRTUAL_HOST: ${VIRTUAL_HOST_NAME}
LETSENCRYPT_HOST: ${SSL_HOST_NAME}
```


`TKVR`
